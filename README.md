# func-obj

> Function over object keys and values into a new object

[![npm](https://img.shields.io/npm/v/func-obj.svg)](https://www.npmjs.com/package/func-obj)
[![pipeline status](https://gitlab.com/sanjorgek/func-obj/badges/master/pipeline.svg)](https://gitlab.com/sanjorgek/func-obj/commits/master)
[![coverage report](https://gitlab.com/sanjorgek/func-obj/badges/master/coverage.svg)](https://gitlab.com/sanjorgek/func-obj/commits/master)
[![node](https://img.shields.io/node/v/func-obj.svg)](sanjorgek.com)
[![npm dev dependency version](https://img.shields.io/npm/dependency-version/func-obj/dev/eslint.svg)](sanjorgek.com)
[![npm bundle size](https://img.shields.io/bundlephobia/min/func-obj.svg)](sanjorgek.com)
[![Snyk Vulnerabilities for npm package](https://img.shields.io/snyk/vulnerabilities/npm/func-obj.svg)](sanjorgek.com)

## Install

```
$ npm install func-obj
```

## Usage

```js
const { map, filter, fold } = require('func-obj');

const newObject = map({foo: 'bar'}, (key, value) => [value, key]);
//=> {bar: 'foo'}

newObject = await map({foo: 'bar'}, (key, value) => Promise.resolve([value, key]), {thenable: true});
//=> {bar: 'foo'}

newObject = filter(object, (key, value) => value === true);
//=> {foo: true}

newObject = await filter(
  {foo: null, bar: {foo: null}},
  (key, value) => new Promise((resolve, reject)=> resolve(value !== null)),
  {deep: true, thenable: true}
);
//=> {foo: true}

newObject = filter({foo: true, bar: false}, ['bar']);
//=> {bar: false}

const obj = { foo: 2, bar: { foo: 1, bar: { foo: 1, bar: 1 } } };
newObject = await fold(obj, (ack, a, b) => new Promise((resolve, reject)=> resolve(ack + a + b)), 0, { deep: true, thenable: true });
// => 5

newObject = fold(obj, (ack, a, b) => ack + b, 0, { deep: true });
// => 5
```

## API

### map(source, mapper, options?)

#### source

Type: `object`

Source object to copy properties from.

#### mapper

Type: `Function`

Mapping function.

- It has signature `mapper(sourceKey, sourceValue, source)`.
- It must
  - return a two item array: `[targetKey, targetValue]`.
  - or a promise that resolve on two item array: `[targetKey, targetValue]`.

#### options

Type: `object`

##### deep

Type: `boolean`<br>
Default: `false`

Recurse nested objects and objects in arrays.

##### target

Type: `object`<br>
Default: `{}`

Target object to map properties on to.

###### thenable

Type: `object`<br>
Default: `false`

Tells if the mapper is handle like a promise or not.

### filter(source, filter, options?)
### filter(source, includeKeys, options?)

#### source

Type: `object`

Source object to filter properties from.

#### filter

Type: `Function`

If is a function, predicate function that determines whether a property should be assigned to the
new object. The function has the signature `filterFunction(sourceKey, sourceValue, source)`. It must
  - return a boolean.
  - or a promise that resolve on a boolean.

#### includeKeys

Type: `string[]`

Array of property names that should be assigned to the new object.

#### options

Type: `object`

##### deep

Type: `boolean`<br>
Default: `false`

Recurse nested objects and objects in arrays.

###### thenable

Type: `object`<br>
Default: `false`

Tells if the mapper is handle like a promise or not.

### fold(source, reduce, accumulator, options?)

#### source

Type: `object`

Source object to fold properties from.

#### reduce

Type: `Function`

- It has signature `reducer(accumulator, key, value, source)`.
- It must
  - return a value that results from the reduction.
  - or a promise that resolve a value that results from the reduction.

#### accumulator

Type: `any`

A value to use as the first argument.

#### options

Type: `object`

##### deep

Type: `boolean`<br>
Default: `false`

Recurse nested objects and objects in arrays.

###### thenable

Type: `object`<br>
Default: `false`

Tells if the mapper is handle like a promise or not.
