module.exports = config => {
  config.set({
    mutator: 'javascript',
    packageManager: 'npm',
    reporters: ['html', 'clear-text'],
    testRunner: 'mocha',
    transpilers: [],
    testFramework: 'mocha',
    mochaOptions: {
      spec: ['test/**/*.js'],
    },
    coverageAnalysis: 'all',
    mutate: ['src/**/*.js'],
    thresholds: { high: 80, low: 60, break: 59 },
  });
};
