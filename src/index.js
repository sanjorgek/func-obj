/* eslint no-param-reassign: [0, {"ignorePropertyModificationsFor": ["seen"] }] */
const Promise = require('bluebird');

// Customized for this use-case
const isObject = x => {
  return typeof x === 'object' && !(x instanceof RegExp)
    && !(x instanceof Error) && !(x instanceof Date);
};

function mapObj(object, fns, options, seen) {
  options = {
    target: {},
    ...options,
  };

  seen = seen || new WeakMap();

  if (seen.has(object)) {
    return seen.get(object);
  }

  seen.set(object, options.target);

  const { target } = options;
  delete options.target;

  const mapArray = (array, gns) => {
    return array.map(x => {
      return isObject(x) ? mapObj(x, gns, options, seen) : x;
    });
  };

  const mapArrayPromise = (array, gns) => {
    return Promise.map(
      array,
      x => {
        return isObject(x) ? mapObj(x, gns, options, seen) : x;
      },
    );
  };

  if (!Array.isArray(fns)) {
    fns = [fns];
  }

  if (options.thenable) {
    if (Array.isArray(object)) {
      return mapArrayPromise(object, fns);
    }
    return Promise.mapSeries(
      Object.keys(object),
      key => {
        const value = object[key];
        let editedKey;
        return Promise.reduce(
          fns,
          (arr, fn) => {
            let [newKey, newValue] = arr;
            return fn(newKey, newValue, object)
              .then(arrRes => {
                if (arrRes) {
                  editedKey = true;
                  [newKey, newValue] = arrRes;
                  if (options.deep && isObject(newValue)) {
                    if (!Array.isArray(newValue)) {
                      return mapObj(newValue, fns, options, seen);
                    }
                    return mapArrayPromise(newValue, [fn]);
                  }
                  return newValue;
                }
                editedKey = false;
                return undefined;
              })
              .then(mapValue => {
                return [newKey, mapValue];
              });
          },
          [key, value],
        ).then(arr => {
          const [newKey, newValue] = arr;
          if (editedKey) target[newKey] = newValue;
        });
      },
    ).then(() => {
      return target;
    });
  }

  if (Array.isArray(object)) {
    return mapArray(object, fns);
  }

  Object.keys(object).forEach(key => {
    const value = object[key];
    let [newKey, newValue] = [key, value];
    let editedKey;
    fns.forEach(fn => {
      const result = fn(newKey, newValue, object);
      if (result) {
        [newKey, newValue] = result;
        editedKey = true;
        if (options.deep && isObject(newValue)) {
          newValue = Array.isArray(newValue)
            ? mapArray(newValue, [fn])
            : mapObj(newValue, fns, options, seen);
        }
      }
    });
    if (editedKey) target[newKey] = newValue;
  });
  return target;
}

function filterObj(object, predicate, options) {
  options = {
    ...options,
  };
  let f = (key, value, subObject) => {
    if (predicate(key, value, subObject)) {
      return [key, value];
    }
    return undefined;
  };
  const isArray = Array.isArray(predicate);
  if (isArray) {
    f = (key, value, subObject) => {
      if (predicate.includes(key)) {
        return [key, value];
      }
      return undefined;
    };
  }
  if (options.thenable) {
    f = (key, value, subObject) => {
      return predicate(key, value, subObject)
        .then(cond => {
          if (cond) {
            return [key, value];
          }
          return undefined;
        });
    };
  }
  return mapObj(
    object,
    f,
    options,
  );
}

function foldObj(object, func, elem, options) {
  options = {
    thenable: false,
    ...options,
  };
  const keys = Object.keys(object);
  if (options.thenable) {
    return Promise.reduce(
      keys,
      (ack, key) => {
        const value = object[key];
        if (options.deep && isObject(value)) {
          return foldObj(value, func, elem, options)
            .then(newValue => {
              return func(ack, key, newValue);
            });
        }
        return func(ack, key, value);
      },
      elem,
    );
  }
  return keys.reduce(
    (ack, key) => {
      let value = object[key];
      if (options.deep && isObject(value)) {
        value = foldObj(value, func, elem, options);
      }
      return func(ack, key, value);
    },
    elem,
  );
}

module.exports = {
  map: mapObj,
  filter: filterObj,
  fold: foldObj,
};
