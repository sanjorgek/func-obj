/* global before after describe it models */
const should = require('should');
const assert = require('assert');

const { fold } = require('../src/index');

describe('Fold', () => {
  describe('Sync functions over Objects', () => {
    it('must compress', () => {
      const obj = { foo: 2, bar: 3 };
      let result = fold(obj, (ack, a, b) => ack + b, 0);
      result.should.be.equals(5);
      result = fold(obj, (ack, a, b) => ack + a + b, 0);
      result.should.be.equals('0foo2bar3');
    });

    it('deep', () => {
      const obj = { foo: 2, bar: { foo: 1, bar: { foo: 1, bar: 1 } } };
      let result = fold(obj, (ack, a, b) => ack + b, 0, { deep: true });
      result.should.be.equals(5);
      result = fold(obj, (ack, a, b) => ack + b, 1, { deep: true });
      result.should.be.equals(8);
    });
  });

  describe('Async functions over Objects', () => {
    it('must compress', async () => {
      const obj = { foo: 2, bar: 3 };
      let result = await fold(obj, (ack, a, b) => new Promise((resolve, reject)=> resolve(ack + b)), 0, { thenable: true });
      result.should.be.equals(5);
      result = await fold(obj, (ack, a, b) => new Promise((resolve, reject)=> resolve(ack + a + b)), 0, { thenable: true });
      result.should.be.equals('0foo2bar3');
    });

    it('deep', async () => {
      const obj = { foo: 2, bar: { foo: 1, bar: { foo: 1, bar: 1 } } };
      let result = await fold(obj, (ack, a, b) => new Promise((resolve, reject)=> resolve(ack + b)), 0, { deep: true, thenable: true });
      result.should.be.equals(5);
      result = await fold(obj, (ack, a, b) => new Promise((resolve, reject)=> resolve(ack + b)), 1, { deep: true, thenable: true });
      result.should.be.equals(8);
    });
  });
});
