/* global before after describe it models */
const should = require('should');
const assert = require('assert');

const { map, filter, fold } = require('../src/index');

it('Must be functions', async () => {
  map.should.be.a.Function();
  filter.should.be.a.Function();
  fold.should.be.a.Function();
});
