/* global before after describe it models */
const should = require('should');
const assert = require('assert');

const { filter } = require('../src/index');

describe('Filter', () => {
  describe('Sync functions over Objects', () => {
    it('function predicate', () => {
      let obj = {foo: true, bar: false};
      Object.keys(filter(obj, () => true)).length.should.be.equals(2);
      Object.keys(filter(obj, () => false)).length.should.be.equals(0);
      Object.keys(filter(obj, (key, value) => value === true)).length.should.be.equals(1);
    });

    it('array predicate', () => {
      const obj = {foo: true, bar: false};
      const filteredKeys = Object.keys(filter(obj, ['foo']));
      filteredKeys.length.should.be.equals(1);
      filteredKeys[0].should.be.equals('foo');
    });

    it('deep option', () => {
      let obj = {foo: null, bar: {foo: null}};
      let result = filter(obj, (key, value) => value !== null, {deep: true});
      Object.keys(result).length.should.be.equals(1);
      result.should.have.key('bar');
      result.bar.should.be.an.Object();
      Object.keys(result.bar).length.should.be.equals(0);
      obj = {foo: {foo: true, bar: false}, bar: {foo: false}};
      result = filter(obj, ['foo'], {deep: true});
      const filteredKeys = Object.keys(result);
      filteredKeys.length.should.be.equals(1);
      filteredKeys[0].should.be.equals('foo');
      result.foo.should.have.keys('foo');
    })
  });

  describe('Async functions over Objects', () => {
    it('function predicate', async () => {
      const obj = {foo: true, bar: false};
      let result = await filter(obj, () => new Promise((resolve, reject)=> resolve(true)), { thenable: true });
      Object.keys(result).length.should.be.equals(2);
      result = await filter(obj, () => new Promise((resolve, reject)=> resolve(false)), { thenable: true });
      Object.keys(result).length.should.be.equals(0);
      result = await filter(obj, (key, value) => new Promise((resolve, reject)=> resolve(value === true)), { thenable: true });
      Object.keys(result).length.should.be.equals(1);
    });

    it('deep option', async () => {
      let obj = {foo: null, bar: {foo: null}};
      let result = await filter(
        obj,
        (key, value) => new Promise((resolve, reject)=> resolve(value !== null)),
        {deep: true, thenable: true}
      );
      Object.keys(result).length.should.be.equals(1);
      result.should.have.key('bar');
      result.bar.should.be.an.Object();
      Object.keys(result.bar).length.should.be.equals(0);
    })
  });
});
