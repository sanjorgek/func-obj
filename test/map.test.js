/* global before after describe it models */
const should = require('should');
const assert = require('assert');

const { map } = require('../src/index');

describe('Map', () => {
  describe('Sync functions over Objects', () => {
    it('Must change null to key', () => {
      map({foo: null}, (key, val) => [val, key]).null.should.be.equals('foo');
      map({foo: null}, [(key, val) => [val, key]]).null.should.be.equals('foo');
      should.not.exist(map({foo: null}, [(key, val) => [val, key], (key, val) => [val, key]]).foo);
    });

    it('Must change keys to snakecase', () => {
      map({foo: 'bar'}, key => [key, 'unicorn']).foo.should.be.equals('unicorn');
      map({foo: 'bar'}, (key, val) => ['unicorn', val]).unicorn.should.be.equals('bar');
      map({foo: 'bar'}, (key, val) => [val, key]).bar.should.be.equals('foo');
      map({foo: 'bar'}, [key => [key, 'unicorn']]).foo.should.be.equals('unicorn');
      map({foo: 'bar'}, [key => [key, 'unicorn'], key => [key, 'centaur']]).foo.should.be.equals('centaur');
      map({foo: 'bar'}, [(key, val) => ['unicorn', val], (key, val) => ['centaur', val]]).centaur.should.be.equals('bar');
      return map({foo: 'bar'}, [(key, val) => [val, key], (key, val) => [val, key]]).foo.should.be.equals('bar');
    });

    it('target option', () => {
    	const target = {};
    	map({foo: 'bar'}, (key, val) => [val, key], {target}).should.eql(target);
    	map({foo: 'bar'}, [(key, val) => [val, key]], {target}).should.eql(target);
    	target.bar.should.be.equals('foo');
    });

    it('deep option', () => {
      const obj = {one: 1, obj: {two: 2, three: 3}, arr: [{four: 4}, 5]};
      const expected = {one: 2, obj: {two: 4, three: 6}, arr: [{four: 8}, 5]};
      const fn = (key, val) => [key, typeof val === 'number' ? val * 2 : val];
      map(obj, fn, {deep: true}).should.eql(expected);
      return map(obj, [fn], {deep: true}).should.eql(expected);
    });

    it('nested arrays', () => {
    	const obj = {arr: [[0, 1, 2, {a: 3}]]};
      const expected = {arr: [[0, 1, 2, {a: 6}]]};
    	const expected2 = {arr: [[0, 1, 2, {a: 8}]]};
      const fn = (key, val) => [key, typeof val === 'number' ? val * 2 : val];
    	const fn2 = (key, val) => [key, typeof val === 'number' ? val + 2 : val];
    	const result1 = map(obj, fn, {deep: true});
      result1.should.eql(expected);
      const result2 = map(result1, [fn2], {deep: true});
      result2.should.eql(expected2);
      const result3 = map(obj, [fn, fn2], {deep: true});
      result3.should.eql(expected2);
    });

    it('handles circular references', () => {
    	const obj = {one: 1, arr: [2]};
    	obj.circular = obj;
    	obj.arr2 = obj.arr;
    	obj.arr.push(obj);

    	const fn = (key, val) => [key.toUpperCase(), val];
    	let actual = map(obj, fn, {deep: true});

    	let expected = {ONE: 1, ARR: [2]};
    	expected.CIRCULAR = expected;
    	expected.ARR2 = expected.ARR;
    	expected.ARR.push(expected);

    	actual.should.eql(expected);

      actual = map(obj, [fn], {deep: true});

    	expected = {ONE: 1, ARR: [2]};
    	expected.CIRCULAR = expected;
    	expected.ARR2 = expected.ARR;
    	expected.ARR.push(expected);

    	actual.should.eql(expected);
    });
  });

  describe('Async functions over Objects', () => {
    it('Must change null to key (promise)', async () => {
      const result = await map({foo: null}, (key, val) => new Promise((resolve, reject)=> resolve([val, key])), { thenable: true });
      (result == null).should.be.equals(false);
      result.should.have.key('null');
      result.null.should.be.equals('foo');
    });

    it('Must change keys to snakecase', async () => {
      let result = await map({foo: 'bar'}, key => Promise.resolve([key, 'unicorn']), { thenable: true });
      result.foo.should.be.equals('unicorn');
      result = await map({foo: 'bar'}, (key, val) => Promise.resolve(['unicorn', val]), { thenable: true });
      result.unicorn.should.be.equals('bar');
      result = await map({foo: 'bar'}, (key, val) => Promise.resolve([val, key]), { thenable: true });
      result.bar.should.be.equals('foo');
      result = await map({foo: 'bar'}, [key => Promise.resolve([key, 'unicorn'])], { thenable: true });
      result.foo.should.be.equals('unicorn');
      result = await map({foo: 'bar'}, [(key, val) => Promise.resolve(['unicorn', val])], { thenable: true });
      result.unicorn.should.be.equals('bar');
      result = await map({foo: 'bar'}, [(key, val) => Promise.resolve([val, key])], { thenable: true });
      result.bar.should.be.equals('foo');
    });

    it('target option', async () => {
    	const target = {};
    	let result = await map({foo: 'bar'}, (key, val) => Promise.resolve([val, key]), { target, thenable: true });
      result.should.eql(target);
    	result = await map({foo: 'bar'}, [(key, val) => Promise.resolve([val, key])], { target, thenable: true });
      result.should.eql(target);
    	target.bar.should.be.equals('foo');
    });

    it('deep option', async () => {
      const obj = {one: 1, obj: {two: 2, three: 3}, arr: [{four: 4}, 5]};
      const expected = {one: 2, obj: {two: 4, three: 6}, arr: [{four: 8}, 5]};
      const fn = (key, val) => Promise.resolve([key, typeof val === 'number' ? val * 2 : val]);
      let result = await map(obj, fn, {deep: true, thenable: true});
      result.should.eql(expected);
      result = await map(obj, [fn], {deep: true, thenable: true});
      result.should.eql(expected);
    });

    it('nested arrays', async () => {
    	const obj = {arr: [[0, 1, 2, {a: 3}]]};
      const expected = {arr: [[0, 1, 2, {a: 6}]]};
    	const expected2 = {arr: [[0, 1, 2, {a: 8}]]};
      const fn = (key, val) => Promise.resolve([key, typeof val === 'number' ? val * 2 : val]);
    	const fn2 = (key, val) => Promise.resolve([key, typeof val === 'number' ? val + 2 : val]);
    	let result = await map(obj, fn, {deep: true, thenable: true});
      result.should.eql(expected);
      let result2 = await map(result, fn2, {deep: true, thenable: true});
      result2.should.eql(expected2);
      let result3 = await map(obj, [fn, fn2], {deep: true, thenable: true});
      result3.should.eql(expected2);
    });

    it('handles circular references', async () => {
    	const obj = {one: 1, arr: [2]};
    	obj.circular = obj;
    	obj.arr2 = obj.arr;
    	obj.arr.push(obj);

    	const fn = (key, val) => Promise.resolve([key.toUpperCase(), val]);
    	let actual = await map(obj, fn, {deep: true, thenable: true});

    	let expected = {ONE: 1, ARR: [2]};
    	expected.CIRCULAR = expected;
    	expected.ARR2 = expected.ARR;
    	expected.ARR.push(expected);

    	actual.should.eql(expected);

      actual = await map(obj, [fn], {deep: true, thenable: true});

    	expected = {ONE: 1, ARR: [2]};
    	expected.CIRCULAR = expected;
    	expected.ARR2 = expected.ARR;
    	expected.ARR.push(expected);

    	actual.should.eql(expected);
    });
  });
});
